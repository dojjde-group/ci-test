#!/usr/bin/env bash

RNDM=$(( $RANDOM % 6 + 1))
TEXT="Text added into ${RNDM}.txt at `date`"
LATESTTAG=`git describe --abbrev=0`

echo $TEXT >> ${RNDM}.txt
echo ""
cat ${RNDM}.txt
sleep 1 

read -p  "Коммитить будем? Y или N " -n 1 ANSWER
echo "" 

if [[ $ANSWER =~ ^[Yy]$ ]]
then
	read -p "Введи трейлер (added changed etc): " TRAILER
	git pull && \
	git add -A && \
	git commit -m "$TEXT" --trailer "${TRAILER}" 
	echo ""
	echo "Последняя версия: $LATESTTAG" 
	echo ""
	read -p  "Введи номер версии: " NEWTAG
	git tag -a $NEWTAG  -m "Tagged commit with ${NETAG}"
fi



