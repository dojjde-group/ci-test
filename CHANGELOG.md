## 0.7.10 (2022-08-16)

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

## 0.7.7 (2022-08-16)

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

## 0.7.7-24-gbdfa83e (2022-08-16)

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

## 0.7.5-19-ga6d7cd8 (2022-08-12)

No changes.

## 0.7.5-18-g0f36f33 (2022-08-12)

No changes.

## 0.7.5-17-g6e8a1de (2022-08-12)

No changes.

## 0.7.5-16-g7f15ecc (2022-08-12)

No changes.

## 0.7.5-15-g8700099 (2022-08-12)

No changes.

## 0.7.5-14-g47f8b73 (2022-08-12)

No changes.

## 0.7.5-13-gc97ae10 (2022-08-12)

No changes.

## 0.7.5-12-gecf247c (2022-08-12)

No changes.

## 0.7.5-11-g915a8bf (2022-08-12)

No changes.

## 0.7.5-10-g0b3be88 (2022-08-12)

No changes.

## 0.7.5-9-gf9b47c5 (2022-08-12)

No changes.

## 0.7.5-8-g4a85632 (2022-08-12)

No changes.

## 0.7.5-7-g34a9460 (2022-08-12)

No changes.

## 0.7.5-6-gf6653bd (2022-08-12)

No changes.

## 0.7.5-5-ge5391f7 (2022-08-12)

No changes.

## 0.7.5-4-gb3de755 (2022-08-12)

No changes.

## 0.7.5-3-g568506b (2022-08-12)

No changes.

## 0.7.5-2-gafa7a0d (2022-08-12)

No changes.

## 0.7.3 (2022-08-11)

No changes.

## 0.7.3-46-gf3c2567 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-45-gac9b9e0 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-44-g6374014 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-44-g82a7e69 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-44-g62f6a75 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-43-g668f04e (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-43-g1bdf652 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-42-g08b399b (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-41-gf6ac16c (2022-08-15)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-40-g40957c8 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-38-gfeb4e43 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-37-g6aa70cc (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-36-g31f57cb (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gf697700 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gebd6d57 (2022-08-15)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### Test of work (1 change)

- [Testing categories in changelog.yml](dojjde-group/ci-test@0cf03699f4175c54bf1b6c7a723f6e6024e8a3ef)
 ([merge request](dojjde-group/ci-test!7))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gdcdc3c8 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gcf77fc2 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gcdbddcb (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-gc621daf (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-ga992705 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-ga58744a (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-ga14caa8 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g0602596 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g3440e6a (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g838a0fd (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g70ead62 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g8daef07 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-27-g005ee6d (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-26-g7e960ab (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-26-g0cf0369 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (2.txt, 5.txt, 6.txt) (1 change)

- [Text added into 1.txt at пятница, 12 августа 2022 г. 11:06:49 (MSK)](dojjde-group/ci-test@71a129242d8c093029067668588e633a69e3d2ab)
 ([merge request](dojjde-group/ci-test!5))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-25-gb9cb645 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-25-g314f9dd (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))

## 0.7.3-25-g71a1292 (2022-08-12)

### Added (1 change)

- [Adding a GIT TRAILER BLEAT'!](dojjde-group/ci-test@6bc6c9362450646d5a418bbae9b72052d3ef9037)
 ([merge request](dojjde-group/ci-test!3))

### fixed some files (1 change)

- [Merge branch 'dev' into 'main'](dojjde-group/ci-test@838a0fda025e19d4f4c14e99352473a9c31473b0)
 ([merge request](dojjde-group/ci-test!4))

### changed files 4.txt, 1.txt, 2.txt (1 change)

- [Text added into 2.txt at пятница, 12 августа 2022 г. 10:53:26 (MSK)](dojjde-group/ci-test@b9cb6452b83513d9a25a69499a49bd1036d8d96b)
 ([merge request](dojjde-group/ci-test!4))

### added a string into 4.txt (1 change)

- [Text added into 4.txt at четверг, 11 августа 2022 г. 15:43:41 (MSK)](dojjde-group/ci-test@58992c063c5d4f3fc4f13fc9a90d3a5f0433b146)
 ([merge request](dojjde-group/ci-test!3))
